# Image Credits

### 1.png
Made by PizzaLovingNerd

### 2.jpg
Made by [jplenio](https://pixabay.com/en/light-bulb-lights-bokeh-energy-3535435/)

### 3.jpg
Made by [FelixMittermeier](https://pixabay.com/en/milky-way-starry-sky-night-sky-star-2695569/)

### 4.jpg
Made by [stevepb](https://pixabay.com/en/landscape-valley-mist-misty-hills-404072/)

### 5.jpg
Made by [FelixMittermeier](https://pixabay.com/en/milky-way-starry-sky-night-sky-star-2675322/)

### 6.jpg
Made by [csreindeer](https://csreindeer.neocities.org/)

### 7.jpg
Made by 5T20

### 8.jpg
Made by [1914761](https://pixabay.com/en/wire-wallpaper-fence-protection-1193051/)

### 9.jpg
Made by [Hermann](https://pixabay.com/en/mountains-morgenrot-alpine-panorama-392669/)

### 10.jpg
Made by [Phil Kallahar](https://www.pexels.com/photo/photo-of-dried-lava-983200/)

## 11.jpg
Made by [Riedelmeier](https://pixabay.com/en/pizza-stone-oven-pizza-stone-oven-1344720/)

Fun fact about 11.jpg. I wasn't orignially going to include this wallpaper, but people in my Discord said I should have a pizza related wallpaper for pizzaOS. So I added this one.